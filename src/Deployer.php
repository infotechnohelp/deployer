<?php

namespace Infotechnohelp\Deployer;

use phpseclib\Crypt\RSA;
use phpseclib\Net\SSH2;

/**
 * Class Deployer
 * @package Infotechnohelp\Deployer
 */
class Deployer
{
    /**
     * @var array
     */
    private $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function deploy(array $postCloneCommands = null, bool $ignoreBackup = false, bool $useHtdocs = false)
    {
        if ($ignoreBackup) {
            echo 'Backup option is ignored';
        }

        $config = $this->config;

        $ssh = new SSH2($config['host'], 22, 10000);


        $key = new RSA();

        $key->loadKey(file_get_contents($config['privateKeyPath']));

        if (!$ssh->login($config['hostUsername'], $key)) {
            exit('Login Failed');
        }

        $actualDirName = ($config['dirName'] === null) ? $config['gitRepositoryName'] : $config['dirName'];
        $actualDirNameHtdocs = $actualDirName;

        if($useHtdocs){
            $actualDirNameHtdocs .= '/htdocs';
        }

        $timestamp = (new \DateTime)->getTimestamp();

        // COMMANDS
        $commands = [
            'cd ' . $config['deployPath'],
        ];

        if ($ignoreBackup) {
            // If actualDirName exists, remove directory
            $commands[] = sprintf(
                'if test -d %s; then rm -rf %s; echo "Previous version was not backed up";fi',
                $actualDirName,
                $actualDirName
            );
        } else {
            // If actualDirName exists, create a backup and remove directory
            $commands[] = sprintf(
                'if test -d %s;then mkdir -p backup/%s/%s; cp -a %s/. backup/%s/%s;' .
                'echo "Backup %s created"; rm -rf %s; fi',
                $actualDirName,
                $actualDirName,
                $timestamp,
                $actualDirName,
                $actualDirName,
                $timestamp,
                $timestamp,
                $actualDirName
            );
        }

        $commands = array_merge($commands, [
            sprintf(
                'git clone http://%s:%s@%s/%s/%s.git %s',
                $config['gitUsername'],
                $config['gitPassword'],
                $config['gitRepositoryServer'],
                $config['gitUsername'],
                $config['gitRepositoryName'],
                $config['dirName']
            ),

            'cd ' . $actualDirNameHtdocs,
        ]);


        $copyFiles = [];
        foreach ($config['copyFiles'] as $fileCopyConfig) {
            $copyFiles[] = sprintf("echo '%s' >%s", file_get_contents($fileCopyConfig[0]), $fileCopyConfig[1]);
        }
        $commands = array_merge($commands, $copyFiles);

        $permissions = [];
        foreach ($config['permissions'] as $permissionConfig) {
            $permissions[] = sprintf("chmod %s %s", $permissionConfig[0], $permissionConfig[1]);
        }
        $commands = array_merge($commands, $permissions);

        $commands = array_merge($commands, $postCloneCommands);

        //var_dump($commands);exit;

        echo $ssh->exec(implode(' && ', $commands));

        $ssh->disconnect();

        unset($ssh);
    }
}
