### ENV

```env
export DEPLOY_HOST='infotechnohelp.com'
export DEPLOY_HOST_USERNAME='virt01'
export DEPLOY_PRIVATE_KEY_PATH='C:/Users/phili/.ssh/zone_id_rsa'
export DEPLOY_PATH='domeenid/www.infotechnohelp.com/test'
export DEPLOY_GIT_PASSWORD='password'
```

### Implementation 'deploy.php'
```php
<?php

require_once('vendor/autoload.php');

$dotenv = new \Dotenv\Dotenv(__DIR__ . DIRECTORY_SEPARATOR . 'config');
$dotenv->overload();

$dotenv->required([
    'DEPLOY_HOST', 'DEPLOY_HOST_USERNAME', 'DEPLOY_PRIVATE_KEY_PATH', 'DEPLOY_PATH', 'DEPLOY_GIT_PASSWORD',
]);

$config = [
    'host' => getenv('DEPLOY_HOST'),
    'hostUsername' => getenv('DEPLOY_HOST_USERNAME'),
    'privateKeyPath' => getenv('DEPLOY_PRIVATE_KEY_PATH'),
    'deployPath' => getenv('DEPLOY_PATH'),
    'dirName' => null,
    'gitRepositoryServer' => 'gitlab.com',
    'gitRepositoryName' => 'protected-app-skeleton',
    'gitUsername' => 'infotechnohelp',
    'gitPassword' => getenv('DEPLOY_GIT_PASSWORD'),
    'copyFiles' => [
        ['config/.env.test', 'config/.env',],
    ],
    'permissions' => [
        ['777', 'bin/cake'],
    ],
];

$postCloneCommands = [
    'composer install --no-progress --no-suggest --no-interaction --no-dev',
    'cd webroot',
    'npm install',
    'cd ..',
    'bin/cake locale js',
    'bin/cake locale js ee_ET',
    'bin/cake locale js ru_RU',
];

$ignoreBackup = (count($argv) > 1) ? filter_var($argv[1], FILTER_VALIDATE_BOOLEAN) : false;

(new \Infotechnohelp\Deployer\Deployer($config))->deploy($postCloneCommands, $ignoreBackup);
```

### Terminal

`php deploy.php <bool>(ignoreBackup)=false`

i.e.

`php deploy.php true`

### Notes

In case of `php deploy.php`, a backup of a current version will be created in `<deployPath>/backup/<actualDirName>/<timestamp>` (if project already exists)

If `$config['dirName'] == null`, it will become an actual directory name (project name)